import cookie from 'react-cookies';
import * as actionTypes from './constants';
import { sortArr } from '../Helper/Helper';

export const initialState = {
	products: [],
	product: null,
	toggleProductModal: false,
	getProductStatus: {
		loading: false,
		success: false,
		error: false
	},
};

function reducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.GET_PRODUCT: {
			//remove cookie which we have set for a specific product details
			if (cookie.load('product')) {
				cookie.remove('product');
			}
			return {
				...state,
				...{
					getProductStatus: {
						loading: true,
						success: false,
						error: false
					}
				}
			};
		}
		case actionTypes.GET_PRODUCT_SUCCESS: {
			return {
				...state,
				...{
					products: action.payload,
					getProductStatus: {
						loading: false,
						success: true,
						error: false
					}
				}
			};
		}
		case actionTypes.GET_PRODUCT_FAIL: {
			return {
				...state,
				...{
					getProductStatus: {
						loading: false,
						success: false,
						error: true
					}
				}
			};
		}

		case actionTypes.TOGGLE_PRODUCT_MODAL: {
			if (state.product != null) {
				return {
					...state,
					...{
						toggleProductModal: !state.toggleProductModal,
						product: null
					}
				};
			}
			return {
				...state,
				...{
					toggleProductModal: !state.toggleProductModal
				}
			};
		}
		default:
			return state;
	}
}

export default reducer;
