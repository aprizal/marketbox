import * as actionTypes from './constants';

export const getProduct = () => ({
	type: actionTypes.GET_PRODUCT
});

export const getProductSuccess = (payloadData) => ({
	type: actionTypes.GET_PRODUCT_SUCCESS,
	payload: payloadData
});

export const getProductFail = () => ({
	type: actionTypes.GET_PRODUCT_FAIL
});

export const toggleProductModal = () => ({
	type: actionTypes.TOGGLE_PRODUCT_MODAL
});