import { put, call, fork, takeEvery, all } from 'redux-saga/effects';

import config from '../cosmic/config';
import cosmic from '../cosmic/cosmic';
import {
	GET_PRODUCT,
} from './constants';
import {
	getProductSuccess,
	getProductFail,
} from './actions';

function* getProductData() {
	try {
		const params = {
			type_slug: 'products'
		};
		const product = yield call(cosmic, 'GET_TYPE', params);
		yield put(getProductSuccess(product !== undefined ? product : []));
	} catch (err) {
		yield put(getProductFail());
	}
}

function* rootSaga() {
	yield all([
		takeEvery(GET_PRODUCT, getProductData),
	]);
}

export default function* rootSagas() {
	yield fork(rootSaga);
}
