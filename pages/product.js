import React, { Component } from 'react';
import { Layout } from 'antd';

import { withReduxSaga } from '../store/store';
import { getProduct } from '../store/actions';

import Wrapper from '../components/wrapper';
import Product from '../components/Product/Product';

const { Content } = Layout;

class Index extends Component {
	static async getInitialProps({ store }) {
		await store.dispatch(getProduct());
	}

	componentDidMount() {
		console.log(window.location.href)
	}

	render() {
		return (
			<Wrapper title="MarketBox - Replace any complicated marketplace">
				<Layout className="layout content-body-adj">
					<Content>
						<Product />
					</Content>
				</Layout>
			</Wrapper>
		);
	}
}

export default withReduxSaga(Index);
