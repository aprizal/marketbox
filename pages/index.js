import React, { Component } from 'react';
import { Layout } from 'antd';

import { withReduxSaga } from '../store/store';
import { getProduct } from '../store/actions';

import Wrapper from '../components/wrapper';
import Home from '../components/Home/Home';

const { Content } = Layout;

class Index extends Component {
	static async getInitialProps({ store }) {
		await store.dispatch(getProduct());
	}

	componentDidMount() {
		console.log(window.location.href)
	}

	render() {
		return (
			<Wrapper title="MarketBox - Replace any complicated marketplace">
				<Layout className="layout content-body-adj">
					<Content>
						<Home />
					</Content>
				</Layout>
			</Wrapper>
		);
	}
}

export default withReduxSaga(Index);
