import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Button, Icon, Card, Tooltip, notification, Spin } from 'antd';
import ImageZoom from 'react-medium-image-zoom';
import { Link } from '../../routes';
import { getProduct } from '../../store/actions';

const { Meta } = Card;
class Product extends Component {
	componentWillReceiveProps(nextProps) {
		console.log('test', nextProps);
	}

	componentDidMount() {
		this.props.getProduct()
	}

	render() {
		const gutters = 16;
		return (
			<div>
				<Row className="main-row-stl">
					<Col span={24}>
						<h1 className="adding-content-header">
							{' '}
							<div style={{ color: '#333333', fontSize: '38px', fontWeight: '500' }}>
								{'  '} List Product
							</div>
						</h1>
						<p
							style={{
								textAlign: 'center',
								paddingTop: '20px'
							}}
						>
						</p>
					</Col>
				</Row>
				<Row gutter={gutters}>
					{this.props.products.map(product => (
						<Col
							className="gutter-row"
							key={product.slug}
							xxl={6}
							xl={6}
							lg={6}
							md={7}
							sm={12}
							xs={24}
						>
							{ product.title }
						</Col>
					))}
				</Row>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	products: state.products,
});

const mapDispatchToProps = dispatch => ({
	getProduct: () => { dispatch(getProduct()) }
});

export default connect(mapStateToProps, mapDispatchToProps)(Product);
