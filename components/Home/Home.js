import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Button, Icon, Card, Tooltip, notification, Spin } from 'antd';
import ImageZoom from 'react-medium-image-zoom';
import { Link } from '../../routes';
import ProductModal from './ProductModal';
import { toggleProductModal, getProduct } from '../../store/actions';

const { Meta } = Card;
class Home extends Component {
	componentWillReceiveProps(nextProps) {
		console.log('test', nextProps);
	}

	componentDidMount() {
		this.props.getProduct()
	}

	render() {
		console.log(this.props)
		const gutters = 16;
		return (
			<div>
				<ProductModal />
				<Row className="main-row-stl">
					<Col span={24}>
						<h1 className="adding-content-header">
							{' '}
							<div style={{ color: '#333333', fontSize: '38px', fontWeight: '500' }}>
								<img
									style={{ height: '52px', width: '52px', marginTop: '-8px' }}
									alt="cosmic_logo"
									src="/static/logo.svg"
								/>
								{'  '} MarketBox Add Your Product To Any MarketPlace
									</div>
							<b style={{ color: '#333333', fontSize: '24px', fontWeight: 'normal' }}>
								Start adding Products to your marketplace!
									</b>
						</h1>
						<p
							style={{
								textAlign: 'center',
								paddingTop: '20px'
							}}
						>
							<Button
								type="primary"
								size="large"
								icon="plus"
								onClick={() => this.props.onToggleProductModal()}
							>
								Add Product
									</Button>
						</p>
					</Col>
				</Row>
				<Row gutter={gutters}>
					{this.props.products.map(product => (
						<Col
							className="gutter-row"
							key={product.slug}
							xxl={6}
							xl={6}
							lg={6}
							md={7}
							sm={12}
							xs={24}
						>
							{ product.title }
						</Col>
					))}
				</Row>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	products: state.products,
});

const mapDispatchToProps = dispatch => ({
	onToggleProductModal: () => dispatch(toggleProductModal()),
	getProduct: () => { dispatch(getProduct()) }
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
