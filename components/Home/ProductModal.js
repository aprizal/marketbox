import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, Button, Form, Input, Icon, Upload } from 'antd';
import { convertToRaw, ContentState, EditorState, convertFromHTML } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import moment from 'moment';

import { toggleProductModal, addProduct, updateProduct } from '../../store/actions';

const FormItem = Form.Item;

class ProductModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			product_id: '',
			title: '',
			code: '',
			quantity: '',
			price: '',
			marketplace: '',
			content: EditorState.createEmpty(),
			featured_image: '',
			is_new: true,
			fileList: [],
			modalState: false,
			productOldData: ''
		};
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.productOldData !== null && this.state.is_new !== false) {
			this.setState({
				title: nextProps.productOldData.title,
				code: nextProps.productOldData.code,
				quantity: nextProps.productOldData.quantity,
				price: nextProps.productOldData.price,
				marketplace: nextProps.productOldData.metadata.marketplace,
				is_new: false,
				modalState: nextProps.toggleProductModalState,
				productOldData: nextProps.productOldData
			});

			const blocksFromHTML = convertFromHTML(nextProps.productOldData.content);
			const edstate = ContentState.createFromBlockArray(
				blocksFromHTML.contentBlocks,
				blocksFromHTML.entityMap
			);
			const editorNewProps = EditorState.createWithContent(edstate);
			if (this.state.content !== editorNewProps) {
				this.setState({
					content: editorNewProps
				});
			}
		} else {
			this.setState({
				modalState: nextProps.toggleProductModalState
			});
		}
	}

	onEditorStateChange = editorState => {
		this.setState({
			content: editorState
		});
	};

	onKeyPress = e => {
		console.log(e.keyCode === 13);
		if (e.keyCode === 13) {
			this.handleSubmit(e);
		} else if (e.keyCode === 27) {
			this.handleClose();
		}
	};

	handleClose = () => {
		this.setState({
			title: '',
			content: EditorState.createEmpty(),
			featured_image: '',
			marketplace: '',
			is_new: true,
			fileList: [],
			modalState: this.props.toggleProductModalState,
			productOldData: ''
		});
		this.props.form.resetFields();
		this.props.onToggleProductModal();
	};

	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFields((err, fieldsValue) => {
			if (!err) {
				const values = {
					...fieldsValue,
					content: draftToHtml(convertToRaw(this.state.content.getCurrentContent())),
					productOldData: this.state.productOldData
				};
				if (this.state.is_new === true) {
					this.props.onProductFormSubmit(values);
				} else {
					this.props.onProductUpdateFormSubmit(values);
				}

				this.handleClose();
			}
		});
	};

	render() {
		const { getFieldDecorator } = this.props.form;

		const formItemLayout = {
			labelCol: {
				xs: { span: 24 },
				sm: { span: 4 }
			},
			wrapperCol: {
				xs: { span: 24 },
				sm: { span: 20 }
			}
		};

		const fileProps = {
			action: '',
			onRemove: file => {
				this.setState(({ fileList }) => {
					const index = fileList.indexOf(file);
					const newFileList = fileList.slice();
					newFileList.splice(index, 1);
					return {
						fileList: newFileList
					};
				});
			},
			beforeUpload: file => {
				if (this.state.fileList.length === 0) {
					this.setState(({ fileList }) => ({
						fileList: [...fileList, file]
					}));
					return false;
				}
			},
			fileList: this.state.fileList,
			accept: 'images/*',
			supportServerRender: true,
			name: 'featured_image'
		};

		return (
			<div>
				<Modal
					title="Add New Product"
					width="65%"
					visible={this.state.modalState}
					onOk={this.handleSubmit}
					okText="Add"
					onCancel={this.handleClose}
					onKeyPress={this.onKeyPress}
				>
					<Form layout="vertical">
					<FormItem {...formItemLayout} label={<span>ID</span>}>
							{getFieldDecorator('product_id', {
								initialValue: this.state.title,
								rules: [
									{
										required: true,
										message: 'Please enter product id',
										whitespace: true
									}
								]
							})(<Input name="product_id" />)}
						</FormItem>
						<FormItem {...formItemLayout} label={<span>Title</span>}>
							{getFieldDecorator('title', {
								initialValue: this.state.title,
								rules: [
									{
										required: true,
										message: 'Please enter product name',
										whitespace: true
									}
								]
							})(<Input name="title" />)}
						</FormItem>
						<FormItem {...formItemLayout} label={<span>Code</span>}>
							{getFieldDecorator('code', {
								initialValue: this.state.title,
								rules: [
									{
										required: true,
										message: 'Please enter product code',
										whitespace: true
									}
								]
							})(<Input name="code" />)}
						</FormItem>
						<FormItem {...formItemLayout} label={<span>Quantity</span>}>
							{getFieldDecorator('quantity', {
								initialValue: this.state.title,
								rules: [
									{
										required: true,
										message: 'Please enter quantity',
										whitespace: true
									}
								]
							})(<Input name="quantity" />)}
						</FormItem>
						<FormItem {...formItemLayout} label={<span>Price</span>}>
							{getFieldDecorator('price', {
								initialValue: this.state.title,
								rules: [
									{
										required: true,
										message: 'Please enter the price',
										whitespace: true
									}
								]
							})(<Input name="price" />)}
						</FormItem>
						<FormItem {...formItemLayout} label={<span>Marketplace</span>}>
							{getFieldDecorator('marketplace', {
								initialValue: this.state.location,
								rules: [
									{
										required: true,
										message: 'Please enter marketplace',
										whitespace: true
									}
								]
							})(<Input name="marketplace" />)}
						</FormItem>
						<FormItem {...formItemLayout} label="Description">
							{getFieldDecorator('content', {
								rules: [
									{
										type: 'object',
										required: true,
										message: 'Please enter description'
									}
								]
							})(
								<Editor
									name="content"
									editorState={this.state.content}
									image={false}
									onEditorStateChange={this.onEditorStateChange}
									toolbar={{
										options: [
											'inline',
											'blockType',
											'fontSize',
											'fontFamily',
											'list',
											'textAlign',
											'colorPicker',
											'link'
										]
									}}
									wrapperClassName="demo-wrapper"
									editorClassName="demo-editor"
								/>
							)}
						</FormItem>
						<FormItem {...formItemLayout} label="Featured Image">
							{getFieldDecorator('featured_image', {
								rules: [
									{
										required: false,
										message: 'Please select featured image'
									}
								]
							})(
								<Upload name="featured_image" {...fileProps}>
									<Button>
										<Icon type="upload" /> Select File
									</Button>
								</Upload>
							)}
						</FormItem>
					</Form>
				</Modal>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	toggleProductModalState: state.toggleProductModal,
	productOldData: state.product
});

const mapDispatchToProps = dispatch => ({
	onToggleProductModal: () => dispatch(toggleProductModal()),
	onProductFormSubmit: payloadData => dispatch(addProduct(payloadData)),
	onProductUpdateFormSubmit: payloadData => dispatch(updateProduct(payloadData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Form.create()(ProductModal));
