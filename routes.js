const routes = (module.exports = require('next-routes')());

routes.add('index', '/');
routes.add('product-detail', '/product-detail/:productId');
